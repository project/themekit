<?php

/**
 * @file
 * Contains form-related hooks and logic.
 */

use Drupal\themekit\T;

/**
 * Implements hook_forms().
 */
function themekit_forms($form_id) {
  $forms = [];
  // Forms where the id begins with 'themekit:' don't need a dedicated form
  // callback, but can use the 'themekit_generic_form' mechanic instead.
  if (0 === strpos($form_id, 'themekit:')) {
    $forms[$form_id] = [
      'callback' => T::c('_themekit_generic_form'),
      // This can be used for hook_form_FORM_ID_alter().
      // E.g. mymodule_form_themekit_generic_form_alter().
      'base_form_id' => 'themekit_generic_form',
    ];
  }
  return $forms;
}

/**
 * Generic form builder callback.
 *
 * This can be used to create forms without their own custom form builder
 * function, allowing the form array to be defined in the place where it is
 * used. Especially, this allows the form array to be defined in a class method,
 * avoiding procedural code.
 *
 * Usage:
 *   $form = [];
 *   $form['amount'] = ['#type' => 'textfield', ..];
 *   $form['submit'] = [..];
 *   drupal_get_form('_themekit_generic_form', $form).
 *
 * @param array $existing_form
 *   The form array passed in from Drupal core.
 * @param array $form_state
 *   The form state passed in from Drupal core.
 * @param array $form
 *   The parameter to use as the actual form array.
 *
 * @return array
 *   Form array sent to Drupal core's form API.
 */
function _themekit_generic_form(array $existing_form, array &$form_state, array $form) {
  return $form;
}


