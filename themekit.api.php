<?php

/**
 * Alter a form that uses the 'themekit_generic_form' mechanism.
 *
 * Forms using this mechanism cannot have a dedicated alter hook based on the
 * form id, because the form id contains the ":" character, and possibly other
 * uncommon characters. Instead, this hook for the base form id should be used.
 *
 * @param array $form
 *   Complete form to be altered.
 * @param array $form_state
 *   Form state.
 * @param $form_id
 *   The form id. If the mechanism is used in the common way, this will begin
 *   with "themekit:".
 *
 * @see \themekit_forms()
 * @see \_themekit_generic_form()
 * @see \Drupal\themekit\GenericForm::build()
 * @see \hook_form_BASE_FORM_ID_alter()
 */
function hook_form_themekit_generic_form_alter(array &$form, array &$form_state, $form_id) {

}
