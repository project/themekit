<?php

namespace Drupal\themekit;

/**
 * Utility class with entry point for the 'themekit_generic_form' mechanism.
 *
 * See https://www.drupal.org/project/themekit/issues/3166926.
 */
class GenericForm {

  /**
   * Builds a form without a dedicated form builder callback.
   *
   * This allows to define forms within other functions or methods, without the
   * need for a dedicated procedural callback. In some cases this is preferable
   * for code cohesion.
   *
   * @param array $form
   *   The form array as it would be returned from a form builder.
   * @param string $form_id_suffix
   *   Id to disambiguate this form from other forms.
   *   This is used as a suffix for the form id.
   *
   * @return array
   *   Render element for the fully processed form.
   *
   * @see \_themekit_generic_form()
   * @see \themekit_forms()
   */
  public static function build(array $form, $form_id_suffix) {
    return drupal_get_form('themekit:' . $form_id_suffix, $form);
  }

}
