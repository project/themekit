<?php

namespace Drupal\themekit\Tests;

use Drupal\themekit\Callback\Callback_ElementReparent;
use Drupal\themekit\T;

class ThemekitUnitTest extends \DrupalUnitTestCase {

  /**
   * Class loading does not work in unit tests.
   *
   * Check the doc comment on the parent method, or see
   * http://drupal.stackexchange.com/questions/49686/drupal-simpletest-class-autoloading-false
   *
   * @see \DrupalUnitTestCase::setUp().
   */
  public function setUp() {
    parent::setUp();

    require_once dirname(__DIR__) . '/Callback/Callback_ElementReparent.php';
    require_once dirname(__DIR__). '/T.php';
  }

  public static function getInfo() {
    // Note: getInfo() strings should not be translated.
    return [
      'name' => 'Themekit unit test',
      'description' => 'Tests utility functions provided by themekit.',
      'group' => 'Themekit',
    ];
  }

  /**
   * Tests the Callback_ElementReparent processor.
   */
  public function testCallbackElementReparent() {

    $element = [
      '#parents' => ['a', 'b', 'c'],
    ];

    $expected = [
      '#parents' => ['a', 'x', 'y'],
    ];

    $processor = new Callback_ElementReparent(2, ['x', 'y']);

    $this->assertIdentical($expected, $processor($element));
  }

  /**
   * Tests the T::*() methods.
   *
   * @throws \Exception
   *   In theme(), theoretically, if called too early in a request.
   *
   * @see \Drupal\themekit\Tests\ThemekitWebTest::testTTheme()
   */
  public function testTMethods() {
    $this->assertIdentical('check_plain', T::c('check_plain'));
    $this->assertIdentical('foo', T::th('theme_foo'));
    $this->assertIdentical('%foo', T::w('foo_load'));
    $this->assertIdentical('foo', T::hook('hook_foo'));
  }

}
