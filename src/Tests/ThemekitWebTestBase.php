<?php

namespace Drupal\themekit\Tests;

/**
 * Base class for web tests in themekit.
 */
abstract class ThemekitWebTestBase extends \DrupalWebTestCase {

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp('themekit');
  }

  /**
   * Builds a form, without the need for a dedicated form builder callback.
   *
   * @param array $form
   *   The form array.
   *
   * @return array|mixed
   *   Processed form array.
   */
  protected function buildForm(array $form) {
    $form_id = '?';
    $form_state = form_state_defaults() + ['values' => []];
    drupal_prepare_form($form_id, $form, $form_state);

    // Clear out all group associations as these might be different when
    // re-rendering the form.
    $form_state['groups'] = [];

    // Return a fully built form that is ready for rendering.
    return form_builder($form_id, $form, $form_state);
  }

  /**
   * Asserts that a render element produces expected output.
   *
   * @param string $expected
   *   Expected html returned from theme().
   * @param string $hook
   *   Theme hook to pass to theme().
   * @param array $variables
   *   Variables to pass to theme().
   *
   * @return bool
   *   TRUE on pass, FALSE on fail.
   *
   * @see assertThemeOutput()
   *   This core function is not as good as this one, I claim.
   */
  protected function assertTheme($expected, $hook, array $variables) {

    $message = ''
      . '<pre>theme(@hook, @variables)</pre>'
      . '';

    $replacements = [
      '@expected' => var_export($expected, TRUE),
      '@hook' => var_export($hook, TRUE),
      '@variables' => var_export($variables, TRUE),
    ];

    try {
      $actual = theme($hook, $variables);

      $replacements['@actual'] = var_export($actual, TRUE);

      if ($actual !== $expected) {
        $success = FALSE;
        $message .= ''
          . '<hr/>'
          . 'Output: <pre>@actual</pre>'
          . '<hr/>'
          . 'Expected: <pre>@expected</pre>'
          . '';
      }
      else {
        $success = TRUE;
        $message .= ''
          . '<hr/>'
          . 'Output: <pre>@expected</pre>'
          . '';
      }
    }
    catch (\Exception $e) {
      $success = FALSE;
      $replacements['@exception'] = _drupal_render_exception_safe($e);
      $message .= ''
        . '<hr/>'
        . 'Exception: @exception'
        . '<hr/>'
        . 'Expected: <pre>@expected</pre>'
        . '';
    }

    return $this->assert(
      $success,
      format_string($message, $replacements));
  }

  /**
   * Asserts that a render element produces expected output.
   *
   * @param string $expected
   *   Expected html to be returned from drupal_render().
   * @param array $element
   *   Render element to pass to drupal_render().
   *
   * @return bool
   *   TRUE on pass, FALSE on fail.
   */
  protected function assertDrupalRender($expected, array $element) {

    $message = ''
      . '<pre>drupal_render(@element)</pre>'
      . '';

    $replacements = [
      '@expected' => var_export($expected, TRUE),
      '@element' => var_export($element, TRUE),
    ];

    try {
      $actual = drupal_render($element);

      $replacements['@actual'] = var_export($actual, TRUE);

      if ($actual !== $expected) {
        $success = FALSE;
        $message .= ''
          . '<hr/>'
          . 'Output: <pre>@actual</pre>'
          . '<hr/>'
          . 'Expected: <pre>@expected</pre>'
          . '';
      }
      else {
        $success = TRUE;
        $message .= ''
          . '<hr/>'
          . 'Output as expected: <pre>@expected</pre>'
          . '';
      }
    }
    catch (\Exception $e) {
      $success = FALSE;
      $replacements['@exception'] = _drupal_render_exception_safe($e);
      $message .= ''
        . '<hr/>'
        . 'Exception: @exception'
        . '<hr/>'
        . 'Expected: <pre>@expected</pre>'
        . '';
    }

    return $this->assert(
      $success,
      format_string($message, $replacements));
  }

}
