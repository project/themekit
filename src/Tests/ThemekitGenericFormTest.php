<?php

namespace Drupal\themekit\Tests;

use Drupal\themekit\GenericForm;

/**
 * Test for the 'themekit_generic_form' mechanism.
 */
class ThemekitGenericFormTest extends ThemekitWebTestBase {

  public static function getInfo() {
    // Note: getInfo() strings should not be translated.
    return [
      'name' => 'Themekit generic form test',
      'description' => 'Tests the \'themekit_generic_form\' mechanism.',
      'group' => 'Themekit',
    ];
  }

  public function testGenericForm() {

    $form = [];
    $form['text'] = [
      '#type' => 'textfield',
      '#title' => 'Text',
    ];
    $form['color'] = [
      '#type' => 'select',
      '#options' => [
        'red' => 'Red',
        'green' => 'Green',
      ]
    ];
    $form = system_settings_form($form);

    $element = GenericForm::build($form, __METHOD__);

    $form_action_plain = check_plain($element['#action']);
    $form_token_plain = check_plain($element['form_token']['#value']);
    $form_build_id_plain = check_plain($element['form_build_id']['#value']);

    $html_expected = <<<EOT
<form action="$form_action_plain" method="post" id="themekitdrupalthemekitteststhemekitgenericformtesttestgenericform" accept-charset="UTF-8"><div><div class="form-item form-type-textfield form-item-text">
  <label for="edit-text">Text </label>
 <input type="text" id="edit-text" name="text" value="" size="60" maxlength="128" class="form-text" />
</div>
<div class="form-item form-type-select form-item-color">
 <select id="edit-color" name="color" class="form-select"><option value="red">Red</option><option value="green">Green</option></select>
</div>
<input type="hidden" name="form_build_id" value="$form_build_id_plain" />
<input type="hidden" name="form_token" value="$form_token_plain" />
<input type="hidden" name="form_id" value="themekit:Drupal\\themekit\\Tests\\ThemekitGenericFormTest::testGenericForm" />
<div class="form-actions form-wrapper" id="edit-actions"><input type="submit" id="edit-submit" name="op" value="Save configuration" class="form-submit" /></div></div></form>
EOT;

    $this->assertDrupalRender($html_expected, $element);
  }

}
