<?php

namespace Drupal\themekit\Tests;

use Drupal\themekit\Callback\Callback_ElementReparent;
use Drupal\themekit\T;

/**
 * Test for various theme hooks and element types.
 */
class ThemekitWebTest extends ThemekitWebTestBase {

  public static function getInfo() {
    // Note: getInfo() strings should not be translated.
    return [
      'name' => 'Themekit web test',
      'description' => 'Tests theme functions provided by themekit.',
      'group' => 'Themekit',
    ];
  }

  public function testThemekitItemContainers() {

    $element = [
      '#theme' => T::th('theme_themekit_item_containers'),
      '#item_attributes' => ['class' => ['field-item']],
      '#first' => 'field-item-first',
      '#last' => 'field-item-last',
      '#zebra' => ['field-item-even', 'field-item-odd'],
      ['#markup' => 'X'],
      ['#markup' => 'Y'],
      ['#markup' => 'Z'],
    ];

    $html_expected = ''
      . '<div class="field-item field-item-even field-item-first">X</div>'
      . '<div class="field-item field-item-odd">Y</div>'
      . '<div class="field-item field-item-even field-item-last">Z</div>'
      . '';

    $this->assertTheme($html_expected, 'themekit_item_containers', ['element' => $element]);

    $element_copy = $element;
    $this->assertDrupalRender($html_expected, $element_copy);

    $element['#item_tag_name'] = false;
    $html_expected = 'XYZ';
    $this->assertTheme($html_expected, 'themekit_item_containers', ['element' => $element]);
  }

  public function testThemekitItemContainersWithContainer() {

    $element = [
      // Outer wrapper <ol>.
      '#type' => T::th('theme_themekit_container'),
      '#tag_name' => 'ul',
      '#attributes' => ['class' => ['menu']],
      // Wrap each item in <li>.
      '#theme' => T::th('theme_themekit_item_containers'),
      '#item_tag_name' => 'li',
      '#zebra' => TRUE,
      // Items.
      ['#markup' => 'X'],
      ['#markup' => 'Y'],
      ['#markup' => 'Z'],
    ];

    $html_expected = ''
      . '<ul class="menu">'
      . '<li class="even">X</li>'
      . '<li class="odd">Y</li>'
      . '<li class="even">Z</li>'
      . '</ul>'
      . '';

    $this->assertDrupalRender($html_expected, $element);
  }

  public function testLinkWrapper() {

    $element = [
      '#type' => T::th('theme_themekit_link_wrapper'),
      '#path' => 'admin',
      'content_0' => [
        '#markup' => 'Adminis',
      ],
      'content_1' => [
        '#markup' => 'tration',
      ],
    ];

    $html_expected = '<a href="/admin">Administration</a>';

    $this->assertDrupalRender($html_expected, $element);
  }

  public function testItemList() {

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#tag_name' => 'ol',
      'item_0' => [
        '#markup' => 'Item 0',
      ],
      'item_1' => [
        '#markup' => 'Item 1',
      ],
    ];

    $html_expected = ''
      . '<ol>'
      . '<li>Item 0</li>'
      . '<li>Item 1</li>'
      . '</ol>'
      . '';

    $this->assertDrupalRender($html_expected, $element);

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#tag_name' => 'ul',
      // No children.
    ];

    // @todo This should return empty html instead.
    $html_expected = ''
      . '<ul>'
      . '</ul>'
      . '';

    $this->assertDrupalRender($html_expected, $element);

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#tag_name' => 'ul',
      // Children produce empty html.
      0 => [
        '#markup' => '',
      ],
      1 => [],
    ];

    // @todo This should return empty string instead.
    $html_expected = ''
      . '<ul>'
      . '</ul>'
      . '';

    $this->assertDrupalRender($html_expected, $element);

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#tag_name' => 'ul',
      // Some of the children produces empty html.
      ['#markup' => 'Item 0'],
      [],
      ['#markup' => 'Item 2'],
      ['#markup' => ''],
    ];

    $html_expected = ''
      . '<ul>'
      . '<li>Item 0</li>'
      . '<li>Item 2</li>'
      . '</ul>'
      . '';

    $this->assertDrupalRender($html_expected, $element);

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#attributes' => ['class' => ['list']],
      '#tag_name' => 'ol',
      'item_0' => [
        '#attributes' => ['class' => ['item-0']],
        '#markup' => 'Item 0',
      ],
      'item_1' => [
        '#markup' => 'Item 1',
      ],
    ];

    $html_expected = ''
      . '<ol class="list">'
      . '<li class="item-0">Item 0</li>'
      . '<li>Item 1</li>'
      . '</ol>'
      . '';

    $this->assertDrupalRender($html_expected, $element);

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#tag_name' => 'ul',
      '#child_attributes' => ['class' => ['item']],
      'item_0' => [
        '#attributes' => ['class' => ['item-0']],
        '#markup' => 'Item 0',
      ],
      'item_1' => [
        // see what happens for duplicate class.
        '#attributes' => ['class' => ['item']],
        '#markup' => 'Item 1',
      ],
    ];

    $html_expected = ''
      . '<ul>'
      . '<li class="item item-0">Item 0</li>'
      . '<li class="item">Item 1</li>'
      . '</ul>'
      . '';

    $this->assertDrupalRender($html_expected, $element);

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#tag_name' => 'ul',
      '#child_attributes' => ['class' => ['item']],
      '#child_attributes_key' => '#item_attributes',
      'item_0' => [
        '#attributes' => ['class' => ['item-0']],
        '#markup' => 'Item 0',
      ],
      'item_1' => [
        '#item_attributes' => ['class' => ['item-1']],
        '#attributes' => ['class' => ['item-1-content']],
        '#theme_wrappers' => ['container'],
        '#markup' => 'Item 1',
      ],
    ];

    $html_expected = ''
      . '<ul>'
      . '<li class="item">Item 0</li>'
      . '<li class="item item-1"><div class="item-1-content">Item 1</div></li>'
      . '</ul>'
      . '';

    $this->assertDrupalRender($html_expected, $element);

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#tag_name' => 'ul',
      '#child_attributes' => ['class' => ['item']],
      '#child_attributes_key' => 'invalid_key',
      'item_0' => [
        '#attributes' => ['class' => ['item-0']],
        '#markup' => 'Item 0',
      ],
      'item_1' => [
        '#item_attributes' => ['class' => ['item-1']],
        '#markup' => 'Item 1',
      ],
    ];

    $html_expected = ''
      . '<ul>'
      . '<li class="item">Item 0</li>'
      . '<li class="item">Item 1</li>'
      . '</ul>'
      . '';

    $this->assertDrupalRender($html_expected, $element);

    $element = [
      '#theme' => T::th('theme_themekit_item_list'),
      '#tag_name' => 'ul',
      '#child_attributes' => ['class' => ['item']],
      '#child_attributes_key' => FALSE,
      'item_0' => [
        '#attributes' => ['class' => ['item-0']],
        '#markup' => 'Item 0',
      ],
      'item_1' => [
        '#item_attributes' => ['class' => ['item-1']],
        '#markup' => 'Item 1',
      ],
    ];

    $html_expected = ''
      . '<ul>'
      . '<li class="item">Item 0</li>'
      . '<li class="item">Item 1</li>'
      . '</ul>'
      . '';

    $this->assertDrupalRender($html_expected, $element);
  }

  public function testSeparatorList() {

    $element = [
      '#theme' => T::th('theme_themekit_separator_list'),
      '#separator' => ' <span class="separator">|</span> ',
      'item_0' => [
        '#markup' => 'Item 0',
      ],
      'item_1' => [
        '#markup' => 'Item 1',
      ],
    ];

    $html_expected = 'Item 0 <span class="separator">|</span> Item 1';

    $this->assertDrupalRender($html_expected, $element);
  }

  public function testStatusMessages() {
    $element = [
      '#theme' => T::th('theme_themekit_status_messages'),
      '#messages' => [
        'status' => [
          'Status 1',
          'Status 2',
        ],
        'warning' => [
          'Warning 1',
        ],
        'error' => [
          'Error 1 with <em>innocent</em> html.',
          'Error 2 with <broken html',
          'Error 3 with <script>alert("dangerous");</script> html.',
          'Error 4 with &lt; html entities.',
        ],
      ],
    ];

    $html_expected = '<div class="messages status">
<h2 class="element-invisible">Status message</h2>
 <ul>
  <li>Status 1</li>
  <li>Status 2</li>
 </ul>
</div>
<div class="messages warning">
<h2 class="element-invisible">Warning message</h2>
Warning 1</div>
<div class="messages error">
<h2 class="element-invisible">Error message</h2>
 <ul>
  <li>Error 1 with <em>innocent</em> html.</li>
  <li>Error 2 with </li>
  <li>Error 3 with alert("dangerous"); html.</li>
  <li>Error 4 with &lt; html entities.</li>
 </ul>
</div>
';

    $this->assertDrupalRender($html_expected, $element);
  }

  public function testThemekitProcessReparent() {

    $form_orig = [
      'group_a' => [
        '#tree' => TRUE,
        'text' => [
          '#type' => 'textfield',
        ],
      ],
    ];

    // First run without any reparenting.
    $form = $this->buildForm($form_orig);
    $this->assertIdentical(['group_a'], $form['group_a']['#array_parents']);
    $this->assertIdentical(['group_a'], $form['group_a']['#parents']);
    $this->assertFalse(isset($form['group_a']['#name']));
    $this->assertIdentical(['group_a', 'text'], $form['group_a']['text']['#array_parents']);
    $this->assertIdentical(['group_a', 'text'], $form['group_a']['text']['#parents']);
    $this->assertIdentical('group_a[text]', $form['group_a']['text']['#name']);

    // Assign THEMEKIT_POP_PARENT.
    $form_orig['group_a']['#process'] = [THEMEKIT_POP_PARENT];

    // Run again with reparented elements.
    $form = $this->buildForm($form_orig);
    $this->assertIdentical(['group_a'], $form['group_a']['#array_parents']);
    $this->assertIdentical([], $form['group_a']['#parents']);
    $this->assertFalse(isset($form['group_a']['#name']));
    $this->assertIdentical(['group_a', 'text'], $form['group_a']['text']['#array_parents']);
    $this->assertIdentical(['text'], $form['group_a']['text']['#parents']);
    $this->assertIdentical('text', $form['group_a']['text']['#name']);

    // Now assign reparent.
    $form_orig['group_a']['#process'][] = new Callback_ElementReparent(1, ['group_b']);

    // Run again with reparented elements.
    $form = $this->buildForm($form_orig);
    $this->assertIdentical(['group_a'], $form['group_a']['#array_parents']);
    $this->assertIdentical(['group_b'], $form['group_a']['#parents']);
    $this->assertFalse(isset($form['group_a']['#name']));
    $this->assertIdentical(['group_a', 'text'], $form['group_a']['text']['#array_parents']);
    $this->assertIdentical(['group_b', 'text'], $form['group_a']['text']['#parents']);
    $this->assertIdentical('group_b[text]', $form['group_a']['text']['#name']);
  }

}
