<?php

namespace Drupal\themekit\Tests;

use Drupal\themekit\T;

/**
 * Test for T::*() methods that cannot be tested in a unit test.
 *
 * Only T::drupal_get_form() is NOT being tested, because the test would be much
 * more complex than the function itself.
 */
class ThemekitTTest extends ThemekitWebTestBase {

  public static function getInfo() {
    // Note: getInfo() strings should not be translated.
    return [
      'name' => 'Themekit T::*() test',
      'description' => 'Tests T::*() methods that cannot be tested in a unit test.',
      'group' => 'Themekit',
    ];
  }

  /**
   * Tests the T::theme() method.
   *
   * @throws \Exception
   *   In theme(), theoretically, if called too early in a request.
   */
  public function testTTheme() {

    $this->assertIdentical(
      '<a href="/admin/structure">Structure</a>',
      T::theme('theme_link', [
        'path' => 'admin/structure',
        'text' => 'Structure',
        'options' => ['attributes' => [], 'html' => FALSE],
      ]));
  }

}
