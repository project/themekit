<?php

use Drupal\themekit\T;

/**
 * @file
 * Contains theme hook default implementations.
 */

/**
 * Theme function for 'themekit_container' theme wrapper.
 *
 * This theme hook should be used in '#theme_wrappers'.
 *
 * @param array $variables
 *   Format:
 *   - $['element']['#tag_name'] = $container_tag_name;
 *   - $['element']['#attributes']['class'][] = $container_class;
 *   - $['element']['#children'] = $rendered_content;
 *
 * @return string
 *   Rendered html.
 *
 * @see theme_container()
 */
function theme_themekit_container($variables) {
  $element = $variables['element'];
  $tagName = isset($element['#tag_name'])
    ? $element['#tag_name']
    : 'div';
  $attributes_str = isset($element['#attributes'])
    ? drupal_attributes($element['#attributes'])
    : '';
  if (empty($element['#children'])) {
    return '';
  }

  return '<' . $tagName . $attributes_str . '>' . $element['#children'] . '</' . $tagName . '>';
}

/**
 * Theme function for 'themekit_link_wrapper' theme wrapper.
 *
 * This theme hook should be used in '#theme_wrappers'.
 *
 * @param array $variables
 *   Format:
 *   - $['element']['#path'] = $link_path;
 *   - $['element']['#children'] = $rendered_link_content;
 *   - $['element']['#options'] = $link_options;
 *   - $['element']['#options']['attributes']['class'][] = $link_class;
 *
 * @return string
 *   Rendered html.
 *
 * @throws Exception
 *   In theme() called by l(), if called too early in the request.
 *
 * @see theme_link()
 */
function theme_themekit_link_wrapper(array $variables) {
  $element = $variables['element'];
  if (!isset($element['#path'])) {
    return '';
  }
  if (empty($element['#children'])) {
    return '';
  }
  $link_options = isset($element['#options'])
    ? $element['#options']
    : [];
  $link_options['html'] = TRUE;
  return l($element['#children'], $element['#path'], $link_options);
}

/**
 * Theme function for 'themekit_item_containers'.
 *
 * Renders a list by wrapping each item in its own html tag.
 * Does NOT add an outer wrapper.
 *
 * @param array $variables
 *   Format:
 *   - $['element']['#item_tag_name'] = 'div';
 *     (tag name for each item)
 *   - $['element']['#item_attributes']['class'][] = $common_item_class;
 *     (attributes to use for each item)
 *   - $['element'][] = $item_element;
 *     (add list items as render element children)
 *
 * @return string
 *   Rendered html.
 */
function theme_themekit_item_containers(array $variables) {

  $element = $variables['element'];

  $deltas = element_children($element);

  if ([] === $deltas) {
    return '';
  }

  if (!isset($element['#item_tag_name'])) {
    $tag_name = 'div';
    $item_close_tag = '</div>';
  }
  elseif (false !== $element['#item_tag_name']) {
    $tag_name = $element['#item_tag_name'];
    $item_close_tag = '</' . $tag_name . '>';
  }
  else {
    $html = '';
    foreach ($deltas as $delta) {
      $html .= drupal_render($element[$delta]);
    }

    return $html;
  }

  $common_attributes = !empty($element['#item_attributes'])
    ? $element['#item_attributes']
    : [];

  if (empty($element['#zebra']) && empty($element['#first']) && empty($element['#last'])) {

    $item_open_tag = [] !== $common_attributes
      ? '<' . $tag_name . drupal_attributes($common_attributes) . '>'
      : '<' . $tag_name . '>';

    $html = '';
    foreach ($deltas as $i => $delta) {
      $html .= $item_open_tag . drupal_render($element[$delta]) . $item_close_tag;
    }

    return $html;
  }

  if (empty($element['#zebra'])) {
    $attributes_by_delta = array_fill_keys($deltas, $common_attributes);
  }
  else {
    $zebra_stripes = is_array($element['#zebra'])
      ? $element['#zebra']
      : ['even', 'odd'];

    $n_zebra_stripes = count($zebra_stripes);

    $attributes_zebra = [];
    foreach ($zebra_stripes as $i => $class) {
      $attributes_zebra[$i] = $common_attributes;
      $attributes_zebra[$i]['class'][] = $class;
    }

    $attributes_by_delta = [];
    foreach ($deltas as $i => $delta) {
      $attributes_by_delta[$delta] = $attributes_zebra[$i % $n_zebra_stripes];
    }
  }

  if (!empty($element['#first'])) {
    $attributes_by_delta[$deltas[0]]['class'][] = is_string($element['#first'])
      ? $element['#first']
      : 'first';
  }

  if (!empty($element['#last'])) {
    $n = count($deltas);
    $attributes_by_delta[$deltas[$n - 1]]['class'][] = is_string($element['#last'])
      ? $element['#last']
      : 'last';
  }

  $html = '';
  foreach ($attributes_by_delta as $delta => $delta_attributes) {
    $item_open_tag = '<' . $tag_name . drupal_attributes($delta_attributes) . '>';
    $html .= $item_open_tag . drupal_render($element[$delta]) . $item_close_tag;
  }

  return $html;
}

/**
 * Theme function for 'themekit_item_list'.
 *
 * Renders a list using ul/li or ol/li tags.
 *
 * @param array $variables
 *   Format:
 *   - $['element']['#tag_name'] = 'ol'|'ul'.
 *     (tag name for outer wrapper)
 *   - $['element']['#attributes']['class'][] = $wrapper_class;
 *     (attributes for the outer ul/ol tag)
 *   - $['element']['#child_attributes']['class'][] = $common_item_class;
 *     (attributes to use for each item)
 *   - $['element'][] = $item_element;
 *     (add list items as render element children)
 *   - $['element']['#child_attributes_key'] = $child_attributes_key;
 *     (key where to look for item-specific attributes)
 *   - $['element'][][$child_attributes_key]['class'][] = $specific_item_class;
 *     (attributes for a specific item)
 *
 * @return string
 *   Rendered HTML.
 */
function theme_themekit_item_list(array $variables) {
  $element = $variables['element'];

  $default_child_attributes = isset($element['#child_attributes'])
    ? $element['#child_attributes'] + []
    : [];
  $default_child_attributes_str = drupal_attributes($default_child_attributes);

  if (!isset($element['#child_attributes_key'])) {
    // Look into $element[$delta]['#attributes'] for each child element.
    $child_attributes_key = '#attributes';
  }
  elseif (empty($element['#child_attributes_key'])) {
    // Child attributes key disabled on purpose.
    // Ignore item attributes defined in the child elements.
    $child_attributes_key = NULL;
  }
  elseif ('#' !== $element['#child_attributes_key'][0]) {
    // Invalid key.
    // Ignore item attributes defined in the child elements.
    $child_attributes_key = NULL;
  }
  else {
    // Look into $element[$delta][$child_attributes_key] for each child element.
    $child_attributes_key = $element['#child_attributes_key'];
  }

  $html = '';
  foreach (element_children($element) as $key) {
    $child_html = drupal_render($element[$key]);
    if ('' === $child_html) {
      continue;
    }
    if (NULL !== $child_attributes_key && !empty($element[$key][$child_attributes_key])) {
      $child_attributes = $element[$key][$child_attributes_key];
      if (isset($child_attributes['class'], $default_child_attributes['class'])) {
        $child_attributes['class'] = array_unique(
          array_merge(
            $default_child_attributes['class'],
            $child_attributes['class']));
      }
      $child_attributes += $default_child_attributes;
      $child_attributes_str = drupal_attributes($child_attributes);
    }
    else {
      $child_attributes_str = $default_child_attributes_str;
    }
    $html .= '<li' . $child_attributes_str . '>' . $child_html . '</li>';
  }
  if (isset($element['#tag_name']) && $element['#tag_name'] === 'ol') {
    $tag_name = 'ol';
  }
  else {
    $tag_name = 'ul';
  }

  $attributes_str = isset($element['#attributes'])
    ? drupal_attributes($element['#attributes'])
    : '';

  return '<' . $tag_name . $attributes_str . '>' . $html . '</' . $tag_name . '>';
}

/**
 * Theme function for 'themekit_separator_list'.
 *
 * @param array $variables
 *   Format:
 *   - $['element']['#separator'] = ' | ';
 *     (separator as html snippet)
 *   - $['element'][] = $item_element;
 *     (add list items as render element children)
 *
 * @return string
 *   Rendered html.
 */
function theme_themekit_separator_list(array $variables) {
  $element = $variables['element'];
  $pieces = [];
  foreach (element_children($element) as $key) {
    $pieces[] = drupal_render($element[$key]);
  }
  $separator = isset($element['#separator'])
    ? $element['#separator']
    : '';
  return implode($separator, $pieces);
}

/**
 * Theme function for 'themekit_status_messages'.
 *
 * Differences to core theme('status_messages'):
 * - Messages are passed in via $variables, instead of being pulled from
 *   drupal_get_messages().
 * - String messages are always sanitized with filter_xss_admin(), independent
 *   of the theme.
 * - Messages can be render elements, in which case the output won't be
 *   sanitized.
 *
 * @param array $variables
 *   Format: $variables['messages'][$type][] = $message,
 *   where $message can be a string or a render array.
 *   If $message is a string, it will be passed through filter_xss_admin().
 *
 * @throws \Exception
 *   In theme(), if called too early in the request.
 *
 * @see \theme_status_messages()
 */
function theme_themekit_status_messages(array $variables) {
  $messages_by_type = [];
  $replacements = [];
  foreach ($variables['messages'] as $type => $messages) {
    foreach ($messages as $delta => $message) {
      if (is_array($message)) {
        // The message is a render element.
        // This is only possible within themekit, not in core.
        $message = drupal_render($message);
        // Prevent some themes from passing this through filter_xss_admin().
        $sanitize = FALSE;
      }
      else {
        // The message should be filtered.
        $sanitize = TRUE;
      }
      if ('' === $message) {
        // The message is empty..
        $messages_by_type[$type][] = '';
        continue;
      }
      $message_sanitized = filter_xss_admin($message);
      if ($message === $message_sanitized) {
        // The filtering makes no difference.
        $messages_by_type[$type][] = $message;
        continue;
      }
      $hex = hash('sha256', "$type:$delta:$message");
      $key = "@($type:$delta:$hex)";
      if ($sanitize) {
        // Insert a key which later will be replaced with the sanitized message.
        $replacements[$key] = $message_sanitized;
        $messages_by_type[$type][] = $key;
      }
      else {
        // Insert a key which later will be replaced with the raw message.
        $replacements[$key] = $message;
        $messages_by_type[$type][] = $key;
      }
    }
  }

  try {
    $messages_by_type_buffer = isset($_SESSION['messages'])
      ? $_SESSION['messages']
      : NULL;
    $_SESSION['messages'] = $messages_by_type;
    $markup = T::theme('theme_status_messages');
    if ($replacements) {
      $markup = strtr($markup, $replacements);
    }
    return $markup;
  }
  finally {
    if ($messages_by_type_buffer !== NULL) {
      $_SESSION['messages'] = $messages_by_type_buffer;
    }
  }
}
